#Relacion Ejercicios XML Acceso a Datos#
###Por Daniel Acedo###

##Ejercicio 1##
Este ejercicio lee un archivo XML conteniendo una lista de empleados. Cada empleado tiene su nombre, su sueldo, su edad y su puesto. Los empleados se listan en un widget ListView.

Además se muestra la edad media y el sueldo máximo y mínimo de los empleados.

![Selección_002.png](https://bitbucket.org/repo/jzMenK/images/1454738209-Selecci%C3%B3n_002.png)

##Ejercicio 2##
La aplicación recoge la predicción del tiempo de AEMET en formato XML de este [link](http://www.aemet.es/xml/municipios/localidad_29067.xml). Una vez leído recoge la información del día de hoy y mañana. Esta información después la muestra en una tabla conteniendo el estado del cielo en las diferentes franjas horarias del día y la temperatura máxima y mínima.

El estado del cielo se muestra usando iconos del servidor de AEMET. El archivo XML contiene el nombre de la imagen y este se sustituye en el siguiente link: http://www.aemet.es/imagenes/png/estado_cielo/11_g.png cambiando 11_g por el codigo en el XML. La imagen se carga usando Picasso.

![Selección_003.png](https://bitbucket.org/repo/jzMenK/images/4012036995-Selecci%C3%B3n_003.png)

##Ejercicio 3##
Esta aplicación lee la información de las estaciones de bici situadas en Zaragoza en XML del siguiente link: http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml . Muestra en una lista todas las estaciones conteniendo su nombre y la fecha de la última actualización de su contenido. También se dispone de un botón para actualizar la información.

![Selección_004.png](https://bitbucket.org/repo/jzMenK/images/1641405516-Selecci%C3%B3n_004.png)

Al hacer click en alguna de las estaciones se abre un actividad conteniendo información más detallada como el estado actual de la estación y el número de bicicletas y anclajes libres. También podremos acceder al mapa de la zona pulsando en el botón inferior que nos llevará al sitio web conteniendo el mapa.

![Selección_005.png](https://bitbucket.org/repo/jzMenK/images/637710243-Selecci%C3%B3n_005.png)

![Selección_006.png](https://bitbucket.org/repo/jzMenK/images/778713921-Selecci%C3%B3n_006.png)

##Ejercicio 4##
En esta aplicación se dispone de una lista de páginas de noticias. Pulsando sobre ellas podremos acceder a la lista de noticias actualizada de cada noticia utilizando su canal RSS mediante un archivo XML.

![Selección_007.png](https://bitbucket.org/repo/jzMenK/images/1360751466-Selecci%C3%B3n_007.png)

Al pulsar sobre un sitio se abrirá una lista de noticias de ese sitio donde podremos ver el título de la noticia y la fecha y hora de publicación. Al pulsar sobre una de ellas se abrirá el navegador web donde podremos ver la noticia completa.

![Selección_008.png](https://bitbucket.org/repo/jzMenK/images/467868187-Selecci%C3%B3n_008.png)

![Selección_009.png](https://bitbucket.org/repo/jzMenK/images/3988393061-Selecci%C3%B3n_009.png)